/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructure.st1264;

import mx.edu.utr.datastructures.List;

/**
 *
 * @author Andrea Gabriela Gonzalez Salas
 */
public class LinkedList implements List{
    
    int size;
    Node head;
    Node tail;
    
    class Node{
        Object element;
        Node next;
        Node previous;
        
        private Node(Object element, Node next, Node previous){
            this.element = element;
            this.next = next;
            this.previous = previous;
        }
    }
    
    public LinkedList(){
        this.head= new Node(null,null,null);
        this.tail = new Node(null,this.head,null);
        this.head.next = this.tail;
    }

     //Appends the specified element to the end of this list.
    @Override
    public boolean add(Object element) {
        addBefore(element,tail);
        return true;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * @param index index at which the specified element is to be inserted.
     * @param element element to be inserted.
     */
    @Override
    public void add(int index, Object element) {
        addBefore(element,getNode(index));
    }

    /**
     * Removes all of the elements from this list. *
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns the element at the specified position in this list.
     */
    @Override
    public Object get(int index) {
        return getNode(index).element;
    }

    /**
     * Returns the index in this list of the first occurrence of the specified
     * element, or -1 if this list does not contain this element.
     */
    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     /**
     * Returns <tt>true</tt> if this list contains no elements.
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    //To remove an element 
    /**
     * Removes the element at the specified position in this list.
     */
    @Override
    public Object remove(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     */
    @Override
    public Object set(int index, Object element) {
        Node n = getNode(index);
        Object old = element;
        n.element = element;
        return old;
    }

    /**
     * Returns the number of elements in this list.
     */
    @Override
    public int size() {
        return size;
    }

    //To add an element before to the end
    private void addBefore(Object element, Node tail) {
        size ++;
    }

    private Node getNode(int index) {
        return null;
    }
    
}
